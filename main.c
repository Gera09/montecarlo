#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-1 //e-7
#define RNMX (1.0-EPS)

typedef struct _ran3NR_Structure{
	long long idum;
	long long idum2;
	long long iy;
	long long iv[NTAB];
}ran2NR_Structure;

typedef ran2NR_Structure* ran2T;

void createRan2T(ran2T r);
float ran2(ran2T r);

int main(){
	ran2T r;
	long long int PointsintoCircle, PointsTotals;
	float x, y, pi;
	int i;
	printf("Paso1\n");
	srand(time(NULL));
	printf("Paso2\n");
	r = malloc(sizeof(ran2NR_Structure));
	printf("Paso3\n");
	omp_set_num_threads(4);
	printf("Paso4\n");
	
	pi=0.0;
	PointsTotals=100000;
	PointsintoCircle = 0;
	#pragma omp parallel
	{
		createRan2T(r);
		#pragma omp for private(i, x, y) firstprivate(r) reduction(+:PointsintoCircle)
		for(i = 0; i < PointsTotals; i++){
			printf("\n\t%lld-\t%lld-\t%lld-\t%lld\n", r-> idum, r-> idum2, r-> iy, r-> iv[NTAB]);
			x = ran2(r);
			y = ran2(r);
			printf("x[%d]: %f\n", i, x);
			printf("y[%d]: %f\n", i, y);
			
			if((x*x + y*y) <= 1){
				PointsintoCircle++;
			}
		}
	}

	printf("\n Puntos en el circulo= %lld",PointsintoCircle);
	printf("\n Puntos totales= %lld",PointsTotals);
	pi=PointsintoCircle;
	pi=pi/PointsTotals;
	
	
	printf("\n Pi= %f \n",pi*4);
	
}

void createRan2T(ran2T r){
	r -> idum = rand();
	r -> idum2 = 123456789;
	r -> iy = 0;
}

float ran2(ran2T r){
	int j;
	long long k;
	double temp;
	
	if(r -> idum <= 0){
		if(-(r -> idum) < 1)
			r -> idum = 1;
		else
			r -> idum = -(r -> idum);
		r -> idum2 = r -> idum;
		for(j = NTAB + 7; j >= 0; j--){
			k = (r -> idum)/IQ1;
			r -> idum = IA1*((r -> idum) - k*IQ1) - k*IR1;
			if(r -> idum < 0)
				r -> idum += IM1;
			if(j < NTAB)
				r -> iv[j] = r -> idum;
		}
		r -> iy = r -> iv[0];
	}
	k = (r -> idum)/IQ1;
	r -> idum = IA1*((r -> idum) - k*IQ1) - k*IR1;
	if(r -> idum < 0)
		r -> idum += IM1;
	k = (r ->idum2)/IQ2;
	r -> idum2 = IA2*((r -> idum2) - k*IQ2) - k*IR2;
	if(r -> idum2 < 0)
		r -> idum2 += IM2;
	j = (r -> iy)/NDIV;
	r -> iy = (r -> iv[j]) - (r -> idum2);
	r -> iv[j] = r -> idum;
	if(r -> iy < 1)
		r -> iy += IMM1;
	if(((temp = AM*(r -> iy)) > RNMX))
		return RNMX;
	else
		return temp;
}

#undef IM1
#undef IM2
#undef AM
#undef IMM1
#undef IA1
#undef IA2
#undef IQ1
#undef IQ2
#undef IR1
#undef IR2
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX